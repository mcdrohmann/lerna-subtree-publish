import { SubtreeConfig, Subtrees } from "../types"
import * as execa from "execa"
import * as path from "path"
import * as fs from "fs"
import { promisify } from "util"
import { gitGetRemotes, gitRemoteAdd, gitTag } from "./git"
import * as color from "colour"

// a short shim that I use because shell.exec(..., {silent: true}) believes that
// it does not always return an ExecOutputReturnValue, so it needs an explicit
// cast.
export const getSubtrees = async (
  cwd: string = process.cwd()
): Promise<Subtrees> => {
  try {
    return JSON.parse(
      await promisify(fs.readFile)(path.join(cwd, "subtrees.json"), "utf8")
    )
  } catch (error) {
    throw new Error(
      `subtrees.json file does not exit, or could not be parsed:\n${error}`
    )
  }
}

export const getSubtreePackageJson = async (
  c: SubtreeConfig,
  ref: string = "HEAD",
  cwd: string = process.cwd()
) => {
  const pkgJsonString = await execa.shell(
    `git show ${ref}:${c.localFolder}/package.json`,
    { cwd }
  )
  return JSON.parse(pkgJsonString.stdout)
}

export const gitSubtreeAdd = async (
  config: SubtreeConfig,
  treeName: string,
  autoFetch: boolean = true,
  cwd: string = process.cwd()
): Promise<boolean> => {
  const localFolderExists = await (async () => {
    try {
      await promisify(fs.readdir)(path.join(cwd, config.localFolder))
      return true
    } catch (error) {
      return false
    }
  })()
  const remotes = await gitGetRemotes(cwd)
  const remoteExists = remotes.indexOf(treeName) !== -1
  if (remoteExists) {
    console.log(`Remote ${color.yellow(treeName)} already exists`)
  } else {
    await gitRemoteAdd(treeName, config.repository, autoFetch, cwd)
  }

  const addSubtreeTags = async (): Promise<boolean> => {
    // Try to add tag of subtree import to repository
    try {
      const version = (await getSubtreePackageJson(config, "HEAD", cwd)).version
      const pkgName = path.basename(config.localFolder)
      const tag = `${pkgName}@${version}`
      console.log(`Add package ${pkgName} with version ${version}`)
      await gitTag(
        tag,
        `Add package ${pkgName} with version ${version}`,
        "HEAD",
        cwd
      )
      return true
    } catch (error) {
      console.log("Repository seems to NOT have a package.json file", error)
      return false
    }
  }
  if (localFolderExists) {
    console.log(
      `Local folder ${color.yellow(config.localFolder)} already exists`
    )
    return addSubtreeTags()
  }
  const res = execa.shell(`git fetch ${treeName}`, { cwd })

  res.stdout.pipe(process.stdout)

  {
    const result = await res

    if (result.code != 0) {
      const errMessage = `Error fetching updates for subtree ${treeName}`
      console.error(errMessage)
      return false
    }
  }

  {
    const result = await execa.shell(
      `git subtree add --prefix=${config.localFolder} ${treeName} ${
        config.branch
      } --squash`,
      { cwd }
    )
    if (result.code != 0) {
      const errMessage = `Error adding subtree ${treeName} at ${
        config.localFolder
      } tracking branch ${config.branch}`
      console.error(errMessage)
      return false
    }
  }

  return addSubtreeTags()
}

export const getSubtreeConfig = async (
  treeName: string,
  cwd: string = process.cwd()
) => {
  const subtrees = await getSubtrees(cwd)
  if (subtrees.hasOwnProperty(treeName)) {
    return subtrees[treeName]
  } else {
    console.error(`Unknown subtree ${treeName}`)
    throw new Error(`Unknown subtree ${treeName}`)
  }
}

export const gitSubtreeSplit = async (
  config: SubtreeConfig,
  ref: string = "master",
  cwd: string = process.cwd()
): Promise<string | undefined> => {
  const res = execa.shell(
    `git subtree split --prefix=${config.localFolder} ${ref} --squash`,
    { cwd }
  )
  res.stdout.pipe(process.stdout)

  const result = await res

  if (result.code !== 0) {
    const errMessage = `Could not split ${config.localFolder}\n:${res.stderr}`
    console.error(errMessage)
    return undefined
  }
  return result.stdout
}

export const gitSubtreePull = async (
  config: SubtreeConfig,
  treeName: string,
  cwd: string = process.cwd()
): Promise<boolean> => {
  return gitSubtreeCmd(
    "pull",
    treeName,
    config.localFolder,
    config.branch,
    "--squash",
    cwd
  )
}

const ensureUpstreamBranchExistsRemotely = async (
  treeName: string,
  downstreamBranch: string,
  upstreamBranch: string,
  cwd: string = process.cwd()
): Promise<boolean> => {
  {
    const res = execa.shell(
      `git ls-remote --heads ${treeName} ${upstreamBranch}`,
      { cwd }
    )

    const result = await res
    const stdoutString = result.stdout.toString().trim()
    if (result.code != 0) {
      const errMessage = `Failed to check for remote branch ${upstreamBranch} on subtree ${treeName}`
      console.error(errMessage)
      return false
    }
    if (stdoutString != "") {
      // upstream branch exists already.
      return true
    }
  }

  console.log(
    `Creating upstream branch ${upstreamBranch} on subtree ${treeName} tracking ${downstreamBranch}`
  )
  {
    const res = execa.shell(`git fetch ${treeName}`)
    res.stdout.pipe(process.stdout)

    const result = await res
    if (result.code != 0) {
      const errMessage = `Failed to fetch updates for subtree ${treeName}`
      console.error(errMessage)
      return false
    }
  }

  const tempLocalUpstreamBranch = `temp/${treeName}/${upstreamBranch}`

  {
    const res = await execa.shell(
      `git branch ${tempLocalUpstreamBranch} ${treeName}/${downstreamBranch}`
    )

    if (res.code != 0) {
      const errMessage = `Failed to create temporary branch ${tempLocalUpstreamBranch}`
      console.error(errMessage)
      return false
    }
  }

  const cleanupLocalUpstreamBranch = async () => {
    const res = await execa.shell(`git branch -d ${tempLocalUpstreamBranch}`)

    if (res.code != 0) {
      const errMessage = `Failed to delete temporary upstream branch ${tempLocalUpstreamBranch}`
      console.error(errMessage)
      return false
    }
    return true
  }

  {
    const res = await execa.shell(
      `git push ${treeName} ${tempLocalUpstreamBranch}:${upstreamBranch}`
    )

    if (res.code != 0) {
      const errMessage = `Failed to create remote upstream branch ${upstreamBranch} on subtree ${treeName}`
      console.error(errMessage)
      // try to clean up
      await cleanupLocalUpstreamBranch()
      return false
    }
  }
  return cleanupLocalUpstreamBranch()
}

export const gitSubtreePush = async (
  config: SubtreeConfig,
  treeName: string,
  cwd: string = process.cwd()
): Promise<boolean> => {
  if (config.upstream === undefined) {
    return gitSubtreeCmd(
      "push",
      treeName,
      config.localFolder,
      config.branch,
      "--squash",
      cwd
    )
  } else {
    const res = await ensureUpstreamBranchExistsRemotely(
      treeName,
      config.branch,
      config.upstream,
      cwd
    )
    if (!res) {
      return false
    }
    return gitSubtreeCmd(
      "push",
      treeName,
      config.localFolder,
      config.upstream,
      "",
      cwd
    )
  }
}

export const gitSubtreeCmd = async (
  command: string,
  treeName: string,
  localFolder: string,
  branch: string,
  toSquashOrNot: string,
  cwd: string = process.cwd()
): Promise<boolean> => {
  const res = execa.shell(
    `git subtree ${command} --prefix=${localFolder} ${treeName} ${branch} ${toSquashOrNot}`,
    { cwd }
  )
  res.stdout.pipe(process.stdout)

  const result = await res

  if (result.code !== 0) {
    console.error(`Could not push to ${treeName}\n:${res.stderr}`)
    return false
  }
  return true
}
