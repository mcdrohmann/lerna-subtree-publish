import { SubtreeConfig } from "../types"
import { gitChangesPending } from "./git"
import { getSubtrees, getSubtreeConfig } from "./gitSubtree"
import { lastPublishedTag } from "./lastPublishedTag"
import * as color from "colour"

export const asyncExecution = async <I>(
  inputs: I[],
  handler: (input: I) => Promise<boolean>,
  parallel: boolean
): Promise<boolean> => {
  if (parallel) {
    return (await Promise.all(inputs.map(handler))).reduce(
      (p, c) => p && c,
      true
    )
  } else {
    for (let index = 0; index < inputs.length; ++index) {
      const success = await handler(inputs[index])
      if (!success) {
        return false
      }
    }
    return true
  }
}

export const commandAll = async (
  handler: (config: SubtreeConfig, treeName: string) => Promise<boolean>,
  skipCurrentTrees: boolean = false,
  cwd: string = process.cwd()
): Promise<boolean> => {
  if (await gitChangesPending(cwd)) {
    return false
  }
  const subtrees = await getSubtrees(cwd)
  return asyncExecution(
    Object.keys(subtrees),
    async treeName => {
      if (
        skipCurrentTrees &&
        (await lastPublishedTag(subtrees, treeName, cwd)).current
      ) {
        console.log(
          `Skipping ${color.yellow(treeName)}: Sub-tree has not changed.`
        )
        return true
      }
      const config = subtrees[treeName]
      return handler(config, treeName)
    },
    false
  )
}

export const commandSingle = async (
  treeName: string,
  handler: (config: SubtreeConfig, treeName: string) => Promise<boolean>,
  cwd: string = process.cwd()
) => {
  if (await gitChangesPending(cwd)) {
    return false
  }
  const config = await getSubtreeConfig(treeName, cwd)
  return await handler(config, treeName)
}
