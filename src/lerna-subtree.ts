#!/usr/bin/env node

import * as yargs from "yargs"
import { SubtreeConfig } from "./types"
import * as color from "colour"
import { commandAll, commandSingle } from "./lib/commandWrapper"
import {
  gitSubtreeAdd,
  getSubtrees,
  gitSubtreePull,
  gitSubtreePush
} from "./lib/gitSubtree"

const commandSingleOrAll = async (
  args: yargs.Arguments,
  handler: (config: SubtreeConfig, treeName: string) => Promise<boolean>
): Promise<boolean> => {
  if (args.all) {
    return commandAll(handler, false)
  } else {
    if (args._.length < 2) {
      console.error(`Either select --all or a sub-tree name`, args)
      return false
    } else {
      return commandSingle(args._[1], handler)
    }
  }
}

yargs
  .usage("$0 [--all] command [options]")
  .describe("all", "apply command to all sub-trees")
  .default("all", false)
  .boolean("all")
  .command("init", "initialize subtrees", {}, async () => {
    return commandAll(gitSubtreeAdd)
  })
  .command(
    "pull",
    "pull from subtree",
    args => {
      // TODO: add an option to pull a specific tag / version
      return args.positional("name", {
        type: "string",
        describe: "sub-tree name"
      })
    },
    async args => {
      const handler = async (c: SubtreeConfig, tn: string): Promise<boolean> => {
        console.log(`pulling subtree ${color.yellow(tn)}...`)
        if(!await gitSubtreePull(c, tn)) {
          return false
        }
        console.log(
          "You might need to call `lerna bootstrap` if the sub-tree has updated."
        )
        return true
      }
      try {
        return commandSingleOrAll(args, handler)
      } catch (err) {
        console.error(color.red(err))
        return false
      }
    }
  )
  .command(
    "push",
    "push to subtree repository",
    yargs => {
      return yargs.positional("name", {
        describe: "sub-tree name",
        default: ""
      })
    },
    async argv => {
      console.log(argv)
      const handler = async (c: SubtreeConfig, tn: string): Promise<boolean> => {
        console.log(`pushing changes to subtree ${color.yellow(tn)}...`)
        return gitSubtreePush(c, tn)
      }
      try {
        return commandSingleOrAll(argv, handler)
      } catch (err) {
        console.error(color.red(err))
        return false
      }
    }
  )
  .command("list", "list all subtree names", {}, () => {
    const subtrees = getSubtrees()
    console.log(
      Object.keys(subtrees)
        .map(s => s)
        .join("\n")
    )
    /*
    const numArgsParsed = args._.length + Object.keys(args).length
    console.log(
      "list",
      args,
      numArgsParsed,
      process.argv.slice(numArgsParsed + 2)
    ) */
  })
  .demandCommand(
    1,
    1,
    "at least one command needs to be specified",
    "only one command allowed"
  ).argv
