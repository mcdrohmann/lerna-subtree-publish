export interface SubtreeConfig {
  readonly localFolder: string
  readonly repository: string
  readonly branch: string
  readonly upstream?: string
}
export interface Subtrees {
  readonly [name: string]: SubtreeConfig
}
