module.exports = {
   preset: 'ts-jest',
   /*testRegex: "(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$",*/
   moduleFileExtensions: [
      "ts",
      "tsx",
      "js"
    ],
    coveragePathIgnorePatterns: [
      "/node_modules/",
      "/__test__/",
      "/test/",
      "/build/"
    ],
    collectCoverage: true,
    testPathIgnorePatterns: [
      "/node_modules/",
      "/dist/",
      "/build/"
    ],
}
